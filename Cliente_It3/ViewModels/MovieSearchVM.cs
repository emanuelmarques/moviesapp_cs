﻿using System;
using System.Collections.Generic;

namespace Cliente_IT3.ViewModels
{
    public class MovieSearchVM
    {
        public string Title { get; set; }
        public string Year { get; set; }


        public string imdbID { get; set; }
        public string Type { get; set; }
        public string Poster { get; set; }
    }
}