﻿using Cliente_IT3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Cliente_IT3.Helpers
{
    public class OMDBApiHttpClient
    {
        public const string WebApiBaseAddress = "http://www.omdbapi.com/";
        public static ApplicationUser userLogged = null;
        public static List<String> role = null;

        public static HttpClient GetClient()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(WebApiBaseAddress);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var session = HttpContext.Current.Session;
            if (session["token"] != null)
            {
                TokenResponse tokenResponse = getToken();
                client.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("bearer", tokenResponse.AccessToken);
            }
            return client;
        }

        public static void storeToken(TokenResponse token)
        {
            var session = HttpContext.Current.Session;
            session["token"] = token;
        }

        public static TokenResponse getToken()
        {
            var session = HttpContext.Current.Session;
            return (TokenResponse)session["token"];
        }

        public static void addUser(ApplicationUser user)
        {
            userLogged = user;
        }

        public static void removeUser()
        {
            userLogged = null;
        }

        public static void addRole(List<String> r)
        {
            role = r;
        }
    }
}