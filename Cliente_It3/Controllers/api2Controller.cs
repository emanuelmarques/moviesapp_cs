﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Cliente_IT3.ViewModels;
using Cliente_IT3.Helpers;
using System.Net.Http;
using Newtonsoft.Json;

namespace Cliente_IT3.Controllers
{
    public class api2Controller : Controller
    {
        // GET: api/api/5
        public async System.Threading.Tasks.Task<string> Index(string search)
        {
            if (search != null)
            {
                var client = OMDBApiHttpClient.GetClient();
                HttpResponseMessage response = await client.GetAsync("?s=" + search);
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();

                    string[] noSearch = content.Split('[');
                    string[] noSearch2 = noSearch[1].Split(']');
                    string array = '[' + noSearch2[0] + ']';

                    var searchResult =
                    JsonConvert.DeserializeObject<List<MovieSearchVM>>(array);


                    //search specific info from each movie
                    List<MovieVM> movies = new List<MovieVM>();
                    foreach (MovieSearchVM movie in searchResult)
                    {

                        HttpResponseMessage response1 = await client.GetAsync("?i=" + movie.imdbID + "&plot=full");
                        if (response1.IsSuccessStatusCode)
                        {
                            string content1 = await response1.Content.ReadAsStringAsync();
                            var result =
                            JsonConvert.DeserializeObject<MovieVM>(content1);

                            //get youtube trailer
                            var youtubeClient = YoutubeApiHttpClient.GetClient();
                            string query = HttpUtility.UrlEncode(movie.Title + " " + movie.Year + " trailer");
                            HttpResponseMessage youtubeResponse = await youtubeClient.GetAsync("search?part=snippet&q=" + query + "&key=AIzaSyDV-YVgrNfGMbSvlWoghgk121fG4L45900");
                            if (youtubeResponse.IsSuccessStatusCode)
                            {
                                string youtubeContent = await youtubeResponse.Content.ReadAsStringAsync();
                                string[] youtubeAdapter = youtubeContent.Split('}');
                                string[] youtubeAdapter2 = youtubeAdapter[1].Split(':');
                                string[] youtubeAdapter3 = youtubeAdapter2[6].Split('"');
                                string youtubeArray = youtubeAdapter3[1];


                                result.youtubeLink1 = "https://www.youtube.com/embed/" + youtubeArray;
                                result.youtubeLink2 = "https://www.youtube.com/watch?v=" + youtubeArray;
                                movies.Add(result);

                            }
                            else
                            {
                                return "An error occurred: " + youtubeResponse.StatusCode;
                            }

                        }
                        else
                        {
                            return "An error occurred: " + response1.StatusCode;
                        }

                    }
                    return JsonConvert.SerializeObject(movies);
                }
                else
                {
                    return "An error occurred: " + response.StatusCode;
                }
            }
          
            return  JsonConvert.SerializeObject(new List<MovieVM>());
        }
    }

}
